# Organisation des fichiers

L'arborescence du projet est la suivante :

* apache2 : contient le fichier de configuration du serveur apache2 dans le LXC,
* spip : contient les fichiers de configuration de spip ;
* sql : content un export de la base de données utilisée par SPIP avec notamment les articles associés aux bons numéro d'identifiants.

# Déploiement sous GNU/Linux Debian

Mettre à jour le système :
```
% apt-get update && apt-get upgrade
```

Installer le moteur du site :
```
% apt-get install apache2 spip php5 php5-gd imagemagick
```

Créer un dossier projet et y cloner le projet git (nous supposons que vous avez un compte Framagit) :
```
% mkdir -p /opt/www
% cd /opt/www
% git clone git@framagit.org:Parinux/www.git
```

Déclarer un nouveau site dans SPIP :
```
% spip_add_site dev.parinux.org

% cd /etc/apache2/sites-available
% ln -s /opt/www/apache2/dev.parinux.org.conf
% a2ensite dev.parinux.org
% systemctl restart apache2
```


Brancher les fichiers du projet sur l'environnement SPIP : (**en cours de rédaction**)
```
% cd /etc/spip/sites-files/dev.parinux.org/
% rmdir squelettes
% ln -s /opt/www/spip/squelettes

% cd /etc/spip/sites-files/dev.parinux.org/config/
% ln -s /opt/www/spip/config/mes_options.php

% cd /var/lib/spip/
% ln -s /opt/www/spip/.htaccess
```

Importer les articles :
```
% cd /opt/www/sql/
% sudo mysql -u user -p spip  < dev.sql
```

